package No1;

public class Max {

	public static <T extends Comparable<T>> T max(T... elements) {
	    T max = elements[0];
	    for (T element : elements) {
	        if (element.compareTo(max) > 0) {
	            max = element;
	        }
	    }
	    return max;
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		System.out.println("Circle Max: " + max(Integer.valueOf(21387), Integer.valueOf(1234), Integer.valueOf(12412) ));
		System.out.println("rectangle Max: " + max(Integer.valueOf(1), Integer.valueOf(125354), Integer.valueOf(2381) ));
		
	}
	}
