package No7;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JButton;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class bb {

	private JFrame frame;
	private JTextField textField1;
	private JTextField textField2;
	private JTextField textFieldAns;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					bb window = new bb();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public bb() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 440, 227);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		textField1 = new JTextField();
		textField1.setBounds(109, 11, 80, 45);
		frame.getContentPane().add(textField1);
		textField1.setColumns(10);
		
		textField2 = new JTextField();
		textField2.setBounds(295, 11, 80, 45);
		frame.getContentPane().add(textField2);
		textField2.setColumns(10);
		
		textFieldAns = new JTextField();
		textFieldAns.setBounds(163, 128, 96, 45);
		frame.getContentPane().add(textFieldAns);
		textFieldAns.setColumns(10);
		
		JLabel result = new JLabel("RESULT");
		result.setFont(new Font("Tahoma", Font.PLAIN, 18));
		result.setBounds(95, 131, 63, 33);
		frame.getContentPane().add(result);
		
		JButton btnNewButton = new JButton("+");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			
				try {
					int num1, num2, ans;
					num1=Integer.parseInt(textField1.getText());
					num2=Integer.parseInt(textField2.getText());
					
					ans = num1+num2;
					textFieldAns.setText(Integer.toString(ans));
				}
				catch(Exception e1){
					
					JOptionPane.showMessageDialog(null, "Enter valid number");
				}
			
			}
		});
		btnNewButton.setFont(new Font("Tahoma", Font.BOLD, 18));
		btnNewButton.setBounds(20, 67, 74, 50);
		frame.getContentPane().add(btnNewButton);
		
		JButton button = new JButton("-");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					int num1, num2, ans;
					num1=Integer.parseInt(textField1.getText());
					num2=Integer.parseInt(textField2.getText());
					
					ans = num1-num2;
					textFieldAns.setText(Integer.toString(ans));
				}
				catch(Exception e1){
					
					JOptionPane.showMessageDialog(null, "Enter valid number");
				}
			}
		});
		button.setFont(new Font("Tahoma", Font.BOLD, 18));
		button.setBounds(115, 67, 74, 50);
		frame.getContentPane().add(button);
		
		JButton btnX = new JButton("*");
		btnX.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					int num1, num2, ans;
					num1=Integer.parseInt(textField1.getText());
					num2=Integer.parseInt(textField2.getText());
					
					ans = num1*num2;
					textFieldAns.setText(Integer.toString(ans));
				}
				catch(Exception e1){
					
					JOptionPane.showMessageDialog(null, "Enter valid number");
				}
			}
		});
		btnX.setFont(new Font("Tahoma", Font.BOLD, 18));
		btnX.setBounds(212, 67, 74, 50);
		frame.getContentPane().add(btnX);
		
		JButton button_2 = new JButton("/");
		button_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					int num1, num2, ans;
					num1=Integer.parseInt(textField1.getText());
					num2=Integer.parseInt(textField2.getText());
					
					ans = num1/num2;
					textFieldAns.setText(Integer.toString(ans));
				}
				catch(Exception e1){
					
					JOptionPane.showMessageDialog(null, "Enter valid number");
				}
			}
		});
		button_2.setFont(new Font("Tahoma", Font.BOLD, 18));
		button_2.setBounds(301, 67, 74, 50);
		frame.getContentPane().add(button_2);
		
		JLabel lblInput = new JLabel("Input1 :");
		lblInput.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblInput.setBounds(20, 11, 79, 33);
		frame.getContentPane().add(lblInput);
		
		JLabel lblInput_1 = new JLabel("Input2 :");
		lblInput_1.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblInput_1.setBounds(212, 11, 79, 33);
		frame.getContentPane().add(lblInput_1);
	}
}

