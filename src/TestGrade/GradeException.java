package TestGrade;

import java.util.Scanner;

public class GradeException {
	public static void main(String[] args) {
	Scanner scan = new Scanner(System.in);
	int ID=1;
	System.out.println("Input grade for Student ID " + ID + " : ");
	String nilai = scan.nextLine();
	
	
		try{
			if(GradeException(nilai) && isInt(nilai) == false) {
					System.out.println("nilai belajar "+ ID + " : " +  nilai.toUpperCase());
			}else if(isInt(nilai)==true) {
				System.out.println("salah format nilai");
			}
		}
		catch(IllegalArgumentException a){
			System.out.println( a.getMessage());
		}	
	}
	public static boolean GradeException(String s) {
		if(s.length() > 1 || s.isEmpty()) {
			throw new IllegalArgumentException("Salah format nilai");
	}return true;
	}
	
	static boolean isInt(String s)
	{
	try {
		int i = Integer.parseInt(s); 
		return true;
	}catch(NumberFormatException er) 
	{
		return false;
	}}
}
